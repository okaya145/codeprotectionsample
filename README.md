Prerequisites
A C++11 or C++14 compatible compiler (i.e. a compiler that is not too old)
Obfuscated strings: no other prerequisite
Obfuscated calls and predicates: You have to install the Boost library. See below
Boost Library
You have to install the Boost library in order to use some features of ADVdetector (it is used by FSM). 

To install Boost:

Debian / Ubuntu: sudo apt-get install libboost-all-dev
Mac OS X: brew install boost
Windows: Download Boost and install it. Then you have to change the Visual Studio project to point to Boost

Download  ADVobfuscator  from https://github.com/andrivet/ADVobfuscator 
 