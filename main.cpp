// To remove Boost assert messages
#if !defined(DEBUG) || DEBUG == 0
#define BOOST_DISABLE_ASSERTS
#endif

#pragma warning(disable: 4503)


#include <iostream>
#include "MetaString.h"
#include "ObfuscatedCall.h"
#include "ObfuscatedCallWithPredicate.h"

using namespace std;
using namespace andrivet::ADVobfuscator;

//char password[] = "password";

int get_password() {
    int auth_ok = 0;
    char buff[16];
    auto password   = DEF_OBFUSCATED("password");
   
    cin>> buff;

    if(strncmp(buff, password.decrypt(), sizeof(password)) == 0)
        auth_ok = 1;

    return auth_ok;
}

void FunctionToProtect()
{
    cout << OBFUSCATED("Enter password: ")<< endl;
}


void success() {
cout << OBFUSCATED("Success! ")<< endl;
     
}


void Doit() {

    //int res = get_password();

   using namespace andrivet::ADVobfuscator::Machine1;
    OBFUSCATED_CALL0(FunctionToProtect);

   auto result = OBFUSCATED_CALL_RET0(int, get_password);
   
    if (result == 0) {
	cout << OBFUSCATED("Failure! ")<< endl;
 
    }
    else{
    success();
    }      
}
 
int main(int, const char *[])
{    
    Doit()    ;
    return 0;
}


